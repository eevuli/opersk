//authors:
//Eeva Ylimäki 246522 eeva.ylimaki@student.tut.fi
// Ville Tyrväinen 298623 ville.tyrvainen@student.tut.fi
//TIE-02206 Project 1

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string>
#include "splitter.cpp"


using namespace std;

class Datehandler
{
public:
    Datehandler(int day, int month, int year);
    string get_weekday(int D, int M, int Y);
    bool check_day();
    int get_day();
    int get_month();
    int get_year();
    Datehandler previous_day();
    Datehandler next_day();
    void print(Datehandler datehandler) const;

private:
    int day_;
    int month_;
    int year_;

};

//Datehandler builder
Datehandler::Datehandler(int day, int month, int year):
    day_(day), month_(month), year_(year){
}

string Datehandler::get_weekday(int D, int M, int Y){
    //--------------------------------//
    // Parameters:                    //
    //     D: day of the month        //
    //     M: month of the year       //
    //     Y: year                    //
    // Returns:                       //
    //     Weekday as a string        //
    //--------------------------------//
        // The formula for calculating a julian day number is ripped from:
        // https://en.wikipedia.org/wiki/Julian_day
    int julian_day_number =
                    (1461 * (Y + 4800 + (M - 14)/12))/4
                      + (367 * (M - 2 - 12 * ((M - 14)/12)))/12
                      - (3 * ((Y + 4900 + (M - 14)/12)/100))/4 + D - 32075;

    int weekday_number = julian_day_number % 7;

    switch ( weekday_number ) {
      case 0:
        return "Monday";
      case 1:
        return "Tuesday";
      case 2:
        return "Wednesday";
      case 3:
        return "Thursday";
      case 4:
        return "Friday";
      case 5:
        return "Saturday";
      case 6:
        return "Sunday";
      default:
        return "?????";
     }


}

//This method sets the value of date object so that nonexisting dates will be rejected.
//Method returns false if the date doesn't exists. It returns true if the date exists.
bool Datehandler::check_day(){

    //Leap year (Year that is divisible by 4 AND not divisible by 100 OR is divisible by 400)
    if (month_ == 2 && (remainder(year_, 4) == 0 && remainder(year_, 100) != 0) || month_ == 2 && remainder(year_,400)== 0 ){
        if (day_ >= 1 && day_ <= 29){
            return true;
        }
        else {
            return false;
        }        
    }

    else if (month_ == 2){
        if (day_ >= 1 && day_ <= 28){
            return true;
        }
        else {
            return false;
        }
    }

    if (month_ == 1|| month_ == 3|| month_==5|| month_ == 7||month_ == 8|| month_ == 10|| month_ ==12){

       if (day_ >=1 && day_ <= 31){
           return true;
       }
       else {
           return false;
       }
    }
    if (month_ == 4|| month_ == 6|| month_ == 9|| month_ == 11){

        if(day_ >= 1 && day_ <=30) {
            return true;
        }
        else {
            return false;
        }
    }

    else {

            return false;
        }
}

//method returns value of the day.
int Datehandler::get_day(){
    return day_;
}

//method returns value of the month.
int Datehandler::get_month(){
    return month_;
}

//method returns value of the year.
int Datehandler::get_year(){
    return year_;
}

//decrementing the date object. Returns object with the new date value.
Datehandler Datehandler::previous_day(){

    Datehandler pre(day_, month_, year_);

    //Previous month
    if (day_ == 1){
        pre.month_ = month_ -1;
        //New year
        if (pre.month_== 0){
            pre.year_ = year_ -1;
            pre.month_= 12;
            pre.day_ = 31;
        }

        if (pre.month_ == 1 || pre.month_ == 3 ||pre.month_ == 5 || pre.month_==  7 || pre.month_== 8 || pre.month_== 10) {
             pre.day_ = 31;
        }
        else if (pre.month_ == 4 || pre.month_ == 6 ||pre.month_ == 9 || pre.month_==  11) {
             pre.day_ = 30;
        }
        else if ((pre.month_ == 2 && (remainder(year_, 4) == 0 && remainder(year_, 100) != 0)) || month_ ==2 && remainder(year_,400)==0 ){
            pre.day_ = 29;
        }
        else if (pre.month_ == 2){
            pre.day_ = 28;
        }
    }

    //when there is no need to change the month.
    else{
        pre.day_ = day_ -1;
    }

    return pre;

}

//Incrementing the date object. Returns object with the new date value.
Datehandler Datehandler::next_day(){

    Datehandler next(day_, month_, year_);

    if (month_ == 1 || month_ == 3 ||month_ == 5 || month_==  7 || month_== 8 || month_== 10) {
         if (day_ == 31){
             next.month_ = month_+1;
             next.day_ = 1;
             }
         else{
             next.day_ = day_ +1;
         }
    }

    else if (month_ == 4 || month_ == 6 ||month_ == 9 || month_==  11) {
         if (day_ == 30){
             next.month_ = month_+1;
             next.day_= 1;
         }
         else{
             next.day_ = day_ +1;
         }
    }

     else if (month_ == 12) {
          if (day_ == 31){
              next.month_ = 1;
              next.day_= 1;
              next.year_ = year_ +1;
          }
          else{
              next.day_ = day_ +1;
          }
    }
    else if ((month_ == 2 && (remainder(year_, 4) == 0 && remainder(year_, 100) != 0) )|| month_ ==2 && remainder(year_,400)==0 ){

        if (day_ == 29){
            next.month_ =month_+1;
            next.day_ = 1;
        }
        else{
            next.day_ = day_+1;
        }
    }
    else if (month_ == 2){
        if (day_ == 28){
            next.month_ =month_+1;
            next.day_ = 1;
        }
        else{
            next.day_ = day_ +1;
        }
    }

    return next;
}

//Printing the date stored in the object on the screen.
void Datehandler::print(Datehandler datehandler) const{
    cout << "Day is "<< day_ <<endl;
    cout << "Month is " << month_<< endl;
    cout << "Year is " << year_ <<endl;
    cout << "Weekday is " << datehandler.get_weekday(day_, month_, year_)<< endl;
    Datehandler pre  = datehandler.previous_day();
    Datehandler next  = datehandler.next_day();
    cout << "The previous date was: " <<pre.get_weekday(pre.day_,pre.month_,pre.year_)<< " " <<pre.get_day() << "." << pre.get_month() << "." << pre.get_year()<<endl;
    cout << "The next date will be: " <<next.get_weekday(next.day_,next.month_, next.year_)<< " " <<next.get_day() << "." << next.get_month() << "." << next.get_year()<<endl;

}


int main()
{
    //starting the program
    string date = " ";
    cout <<"--------------------------------" <<endl;
    cout <<"Welcome to date testing program!" <<endl;
    cout <<"--------------------------------" <<endl;

    while (true) {
        cout << "Input a date in day-month-year format: ";
        getline (cin, date);

        if (date == "quit"){
            cout << "Test program ending, goodbye!" <<endl;
            return false;
        }
        else{

            cout <<"--- TEST OUTPUT BEGIN" <<endl;

            //Splitter splits users date-input with the sign '-'.
            Splitter splitter_object(date);

            splitter_object.split('-');  // Single quotes: parameter is of type char.

            //Here we check that user input is valid and lenght of fields is correct.
            //If there is frong input, program gives an error.
            if (splitter_object.number_of_fields() != 3){
                cout << "Error: this is not a valid date!" << endl;
            }
            else if ( splitter_object.fetch_field(0).length() >= 3 || splitter_object.fetch_field(0).length() <= 0){
                cout << "Error: this is not a valid date!" << endl;

            }
            else if (splitter_object.fetch_field(1).length() >= 3 ||  splitter_object.fetch_field(1).length() <= 0){
                cout << "Error: this is not a valid date!" << endl;
            }
            else if (splitter_object.fetch_field(2).length() != 4){
                cout << "Error: this is not a valid date!" << endl;
            }

            else{

                //Stoi changes string to int so that object can handle the values of the date.
                int day = stoi(splitter_object.fetch_field(0));
                int month = stoi(splitter_object.fetch_field(1));
                int year = stoi(splitter_object.fetch_field(2));

                //Creating datehandler object.
                Datehandler datehandler(day, month, year);

                //Program calls check_day method
                if (datehandler.check_day() == true){
                    cout << "The date is a valid date." <<endl;
                    datehandler.get_day();
                    datehandler.get_month();
                    datehandler.get_year();
                    datehandler.print(datehandler);

                }

                //date is not correct
                else{
                    cout << "Error: this is not a valid date!" <<endl;
                }
            }

            //ending the test
            cout << "--- TEST OUTPUT END" <<endl;
            cout << " " << endl;
        }
    }
    return 0;
}


